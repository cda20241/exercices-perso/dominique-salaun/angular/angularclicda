import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonComposantBisComponent } from './mon-composant-bis.component';

describe('MonComposantBisComponent', () => {
  let component: MonComposantBisComponent;
  let fixture: ComponentFixture<MonComposantBisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MonComposantBisComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MonComposantBisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
