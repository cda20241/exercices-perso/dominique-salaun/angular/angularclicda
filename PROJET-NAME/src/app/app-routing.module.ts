import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MonComposantComponent } from './mon-composant/mon-composant.component';
import { MonComposantBisComponent } from './mon-composant-bis/mon-composant-bis.component';
import { MonComposantTierComponent } from './mon-composant-tier/mon-composant-tier.component';

const routes: Routes = [
  {path:'',component: MonComposantComponent},
  {path:'moncomposantbis',component: MonComposantBisComponent},
  {path:'moncomposanttier',component: MonComposantTierComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
