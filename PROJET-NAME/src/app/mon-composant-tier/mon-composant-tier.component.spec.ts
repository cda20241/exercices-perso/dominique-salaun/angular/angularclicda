import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonComposantTierComponent } from './mon-composant-tier.component';

describe('MonComposantTierComponent', () => {
  let component: MonComposantTierComponent;
  let fixture: ComponentFixture<MonComposantTierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MonComposantTierComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MonComposantTierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
