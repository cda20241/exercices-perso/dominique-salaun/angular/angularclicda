import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MonComposantComponent } from './mon-composant/mon-composant.component';
import { MonComposantBisComponent } from './mon-composant-bis/mon-composant-bis.component';
import { MonComposantTierComponent } from './mon-composant-tier/mon-composant-tier.component';

@NgModule({
  declarations: [
    AppComponent,
    MonComposantComponent,
    MonComposantBisComponent,
    MonComposantTierComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
